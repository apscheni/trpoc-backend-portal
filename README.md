# How to install

### 1. git clone

```git@bitbucket.org:apscheni/trpoc-backend-portal.git
```

### 2. Install Bootstrap with Bower:

```bower install bootstrap#v4.0.0-alpha.6```


### 3. Install Font Awesome with Bower:

```bower install fontawesome```

### 4. Should work now.


# Notes

- every main-div has a individual class
- some buttons have individual classes
- every modal popup is directly a child of body
- orderView Popup has id=orderView -> load dynamically to avoid multiple id's with same name
- tables with modal popups (e.g. order view) have additional class (has-view)
- mobileNav: onResize >= 992px close mobileNav


# ToDo Artur

## style
- notification dropdown (+content)
- active bar for main- and subnav
- checkboxes as designed
- customs-forms included for select-style (checkbox' are browser default so far)
- fix width for first column if content == checkbox
- more status (e.g. at returns page), artjom's feedback needed
- mobileNav: animationIn from top right

- oultine button hover -> https://mjml.io/
- svg icon animation hover -> http://introtoicons.com/


## check
- check ie9
- check mixins imports
