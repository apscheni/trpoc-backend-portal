$(document).ready(function() {
  // remove modal-backdrop background color only for mobileNav
  $('#mobileNav').on('show.bs.modal', function(e) {
    $('body').addClass('mobileMenuBackdrop');
  });
  $('#mobileNav').on('hidden.bs.modal', function(e) {
    $('body').removeClass('mobileMenuBackdrop');
  });
});

// Hide Header on on scroll down
var didScroll;
var lastScrollTop = 0;
var delta = 25;
var navbarHeight = $('.mainnav-subnav').outerHeight();

$(window).scroll(function(event) {
  didScroll = true;
});

setInterval(function() {
  if (didScroll) {
    hasScrolled();
    didScroll = false;
  }
}, 250);

function hasScrolled() {
  var st = $(this).scrollTop();

  // Make sure they scroll more than delta
  if (Math.abs(lastScrollTop - st) <= delta)
    return;

  // If they scrolled down and are past the navbar, add class .nav-hidden.
  // This is necessary so you never see what is "behind" the navbar.
  if (st > lastScrollTop && st > navbarHeight) {
    // Scroll Down
    $('.mainnav-subnav').addClass('nav-hidden');
  } else {
    // Scroll Up
    if (st + $(window).height() < $(document).height()) {
      $('.mainnav-subnav').removeClass('nav-hidden');
    }
  }

  lastScrollTop = st;
}
